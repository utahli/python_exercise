'''hh'''
massage = "asdf dsf "
print(massage.title())
# print(massage.upper())
# print(massage.lower())

# print(massage.rstrip()) #去除字符串末尾空白
# print(massage.lstrip()) #去除字符串开头空白
# print(massage.strip())  #去除字符串首尾空白

# age = 23
# ms = "happy " + str(age) + "rd Brithday"
# print(ms)

# print(3 / 2) # Py2 : 1; Py3 : 1.5

# lists = ['aa','bb','cc','dd']
# print(lists)

# lists[0] = 'ee'
# print(lists)

# lists.append('ff')
# print(lists)

# lists.insert(0 , 'gg')
# print(lists)

# del lists[0]
# print(lists)

# poped_item = lists.pop()
# print(lists)
# print(poped_item)

# poped_item2 = lists.pop(0)
# print(lists)
# print(poped_item2)

# print("############################3")

# list2 = ['ss','dd','ee','aa']
# print("original list")
# print(list2)

# print("sorted list")
# print(sorted(list2))

# print("original list")
# print(list2)

# print("reverse list")
# list2.reverse()
# print(list2)

# print("original list")
# print(list2)

# listL = ['gg','ff','dd','ss','aa']
# for ls in listL:
#     print(ls)

# range()函数指定步长未2
# enven_number = list(range(1,10,2))
# print(enven_number)

# print(min(enven_number))
# print(max(enven_number))
# print(sum(enven_number))

# squares = [value**2 for value in range(1,9)]
# print(squares)

# #列表切片
# print(squares[1:3])
# print(squares)
# print(squares[-3:])

# list1 = ['gg', 'ff', 'dd', 'ss', 'aa']
# list2 = list1[:]    #两个对象两个引用
# list3 = list1       #一个对象两个引用

# 元组，值不可变
# dimensiona = (200,100)

# requested_toppings = ['mushrooms', 'onions', 'pineapple']
# print('onions' in requested_toppings)

# for item in requested_toppings:
#     if item == 'mushrooms':
#         print(1)
#     elif item == 'onions':
#         print(2)
#     else:
#         print(3)

# list1 = []
# if list1:
#     print(1)
# else:
#     print(2)

# user_0 = {
#     'username': 'efermi',
#     'first': 'enrico',
#     'last': 'fermi',
# }
# for key, value in user_0.items():
#     print("\nKey: " + key)
#     print("Value: " + value)
# print(user_0.keys())
# print(sorted(user_0.keys()))
# print(user_0.values())

# # 数据结构 : list<Map<,>> || Map<,list> || Map<,Map<,>>
# alien_0 = {'color': 'green', 'points': 5}
# alien_1 = {'color': 'yellow', 'points': 10}
# alien_2 = {'color': 'red', 'points': 15}
# aliens = [alien_0, alien_1, alien_2]

# age = input("How old are u ? ")
# print(age) #输入的age为字符串，字符串不能直接与数字进行比较，可以使用int("str")函数将字符串转为数据类型

# with open("D:\\Z_Jars_Classes\\file01.txt") as file_object:
#     contents = file_object.read()
# print("contents.rstrip()")
